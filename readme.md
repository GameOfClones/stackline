> Requirements to Run

Docker (docker-compose/docker-engine)

jdk8

sbt (Scala Build Tool, `brew install sbt@1` on osx)


Navigate to project root and run `docker-compose up`

Project binds to port 8080.  Typically being behind a balancer I use this for services that will have the SSL terminated in front of it then forward 443/80 to 8080.

I can supply the actual docker file it is just huge in comparison to building with the dockerfile.

> Endpoints

prefix = "/api/product/"

GET {prefix}/autocomplete?type=<>&searchTermPrefix=<>

  * Types Accepted: brand, category, title
	
POST {prefix}/search

  * JSON Input Format
   
   { "conditions": [
            { "fieldName": "brand", "values": ["Brother", "Canon"] },
            { "fieldName": "category", "values": ["Printers & Scanners"] }
      ],
      "pagination": { "from": 1, "size": 50 }
    }
  
 * fieldName: productId, title, brand, brandId, category, categoryId
    
    
> General Project Stuff
    
It's 50k items over a small file, however full streaming is definitely possible here.   Spinning up the resources would take longer than just parsing the very small file.
I am definitely able to supply what this would look like stream processed for large data sets.

The exception encoder is with the routes.  This allows us to catch any time we throw an exception down stream and transform it to be something nice and pretty for the end user or debugging. We can find specific errors and provide specific returns there with a simple match typed statement.   

The routes are returning futures as this is how it would be in a real world application.   It is using Twitter futures because they are nice and neat within the Finch/Finagle world, but it can definitely be done using normal Scala Futures as well.

The search made some assumptions that can be easily changed.  They are as follows: productId, brandId, and categoryId try and find exact matches instead of contains.  This logic is easily changed.   The title, brand, and category fields only hope to contain what is being searched.  case insensitive on both regards.

I actually completed this when it was the 8 hour challenge as an exercise.   It took me about 3.5 hours off and on throughout the day on June 12th when Ryan sent it to me for general purposes as the new one was being developed.