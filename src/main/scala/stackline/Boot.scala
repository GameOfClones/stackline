package stackline

import com.twitter.finagle._
import com.twitter.util.{Await, Future}
import stackline.handlers._
import stackline.models._
import io.finch._
import io.finch.syntax._
import io.finch.circe._
import io.circe.generic.auto._
import io.circe._

object Boot
  extends App {
  implicit val encodeExceptions: Encoder[Exception] = Encoder.instance(e =>
    Json.obj("message" -> Option(e.getMessage).fold(Json.Null)(Json.fromString))
  )

  // Initialize the Products List So The First Request Doesn't Do It.
  Products.all

  // API
  val prefix = "api" :: "product"

  val autoComplete: Endpoint[AutoCompleteResult] =
    get(prefix :: "autocomplete" :: paramOption("type") :: paramOption("searchTermPrefix")) {
      (searchType: Option[String], searchTerm: Option[String]) =>

        if (searchType.forall(_.isEmpty)) {
          Future.exception(new Exception("Missing Type"))
        } else if (searchTerm.forall(_.isEmpty)) {
          Future.exception(new Exception("Missing Search Term"))
        } else {
          AutoComplete(searchType.get, searchTerm.get).map(Ok(_))
        }
    }.handle {
      case err: Exception => BadRequest(err)
    }

  val search: Endpoint[SearchResult] =
    post(prefix :: "search" :: jsonBody[SearchRequest]) {
      (searchRequest: SearchRequest) =>
        val searchResult = Search(
          searchRequest.conditions.getOrElse(List.empty),
          searchRequest.pagination.map(_.size).getOrElse(50),
          searchRequest.pagination.map(_.from).getOrElse(1)
        )

        searchResult.map(Ok(_))
    }.handle {
      case err: Exception => BadRequest(err)
    }

    val routes = autoComplete :+: search

    Await.ready(Http.server.serve(s":8080", routes.toService))
}
