package stackline.models

import io.circe.generic.auto._

case class SearchResult(
                         products: List[Product],
                         pagination: Pagination
                       )
