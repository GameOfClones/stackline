package stackline.models

import io.circe.generic.auto._

case class AutoCompleteResult(
                             `type`: String,
                             suggestions: Set[Suggestion]
                             )