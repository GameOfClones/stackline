package stackline.models

case class Pagination(
                       from: Int = 1,
                       size: Int = 50
                     )
