package stackline.models

case class ProductConditions(
                              fieldName: String,
                              values: List[String]
                            )