package stackline.models

case class SearchRequest(
                        conditions: Option[List[ProductConditions]],
                        pagination: Option[Pagination]
                        )
