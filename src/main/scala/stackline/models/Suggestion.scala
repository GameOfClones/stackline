package stackline.models

case class Suggestion(
                       id: String,
                       name: String
                     )