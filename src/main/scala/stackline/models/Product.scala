package stackline.models

final case class Product(
                        productId: String,
                        title: String,
                        brandId: String,
                        brandName: String,
                        categoryId: String,
                        categoryName: String
                        )

object Product {
  val lookups = Seq("brand", "category", "title")
}
