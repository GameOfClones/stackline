package stackline.handlers

import com.twitter.util.Future
import stackline.Products
import stackline.models._

import scala.collection.mutable.ListBuffer

object Search {
 def apply(conditions: List[ProductConditions], size: Int, from: Int): Future[SearchResult] = Future {
   val pIter = Products.all.drop(from-1).toIterator
   val entries = new ListBuffer[Product]
   var found = 0

   while (pIter.hasNext && found < size) {
     val product = pIter.next()

     if (conditions.forall { productCondition =>
       productCondition.values.forall { value =>
         val lowered = value.toLowerCase

         productCondition.fieldName match {
           case "productId" => product.productId.equalsIgnoreCase(lowered)
           case "title" => product.title.toLowerCase.contains(lowered)
           case "brand" => product.brandName.toLowerCase.contains(lowered)
           case "brandId" => product.brandId.equalsIgnoreCase(lowered)
           case "category" => product.categoryName.toLowerCase.contains(lowered)
           case "categoryId" => product.categoryId.equalsIgnoreCase(lowered)
           case _ => false
         }
       }
     }) {
       entries += product
       found += 1
     }
   }

   SearchResult(entries.toList, Pagination(from, entries.size))
 }
}
