package stackline.handlers

import com.twitter.util.Future
import stackline.Products
import stackline.models.{AutoCompleteResult, Product, Suggestion}

object AutoComplete {
  def apply(lookupType: String, searchTermPrefix: String): Future[AutoCompleteResult] = Future {
    if (!Product.lookups.contains(lookupType)) {
      throw new Exception("Invalid field for lookup")
    } else {
      val filteredProducts = (if (searchTermPrefix.length < 11) {
        lookupType match {
          case "brand" => Products.getAutoComplete("brand", searchTermPrefix)
          case "category" => Products.getAutoComplete("category", searchTermPrefix)
          case "title" => Products.getAutoComplete("title", searchTermPrefix)
          case _ => List.empty[Product]
        }
      } else {
        val loweredTerm = searchTermPrefix.toLowerCase

        Products.all.filter { product =>
          lookupType match {
            case "brand" => product.brandName.toLowerCase().startsWith(loweredTerm)
            case "category" => product.categoryName.toLowerCase().startsWith(loweredTerm)
            case "title" => product.title.toLowerCase().startsWith(loweredTerm)
          }
        }
      }).map { product =>
        lookupType match {
          case "brand" => Suggestion(product.brandId, product.brandName)
          case "category" => Suggestion(product.categoryId, product.categoryName)
          case "title" => Suggestion(product.productId, product.title)
        }
      }

      AutoCompleteResult(lookupType, filteredProducts.toSet)
    }
  }
}
