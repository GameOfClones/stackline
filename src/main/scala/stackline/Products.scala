package stackline

import stackline.models.Product
import stackline.util.Parsers
import stackline.util.clients.S3Client

object Products
  extends S3Client
  with Parsers {
  /*
   *  Grab Stream From S3
   *  Decompress Tar
   *  Parse The TSV
   *  50k Lines doesnt really require stream processing, but it very easily can be done here
   */
  private val products = (for {
    configInputStream <- getObject("stackline-public", "sample_product_data.tsv.gz")
    configRaw         <- decompress(configInputStream)
    productsList      <- parseProductTsv(configRaw)
  } yield productsList).getOrElse(List.empty[Product])

  private def mapEntities(entities: List[(String, Product)]): Map[String, List[Product]] = {
    val maxLength = entities.map(_._1.length).max match {
      case i if i < 11 => i
      case _ => 10
    }

    (1 to maxLength).flatMap { i =>
      entities.map { case (label, entity) => (label.slice(0, i).toLowerCase, entity) }
        .groupBy(_._1)
        .mapValues(_.map(_._2))
    }.toMap
  }

  private val brands = mapEntities(products.map(e => (e.brandName, e)))
  private val categories = mapEntities(products.map(e => (e.categoryName, e)))
  private val titles = mapEntities(products.map(e => (e.title, e)))

  def all = products

  def getAutoComplete(entity: String, key: String): List[Product] = {
    val lowered = key.toLowerCase

    (entity.toLowerCase match {
      case "brand"    => brands.get(lowered)
      case "category" => categories.get(lowered)
      case "title"    => titles.get(lowered)
      case _          => None
    }).getOrElse(List.empty[Product])
  }
}
