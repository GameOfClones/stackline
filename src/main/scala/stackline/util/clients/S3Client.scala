package stackline.util.clients

import java.io.InputStream
import java.util.zip.GZIPInputStream

import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.util.IOUtils

import scala.util.Try

trait S3Client {
  private val client = AmazonS3ClientBuilder.standard()
    .withRegion(Regions.US_WEST_2)
    .build()

  def getObject(bucket: String, key: String): Option[InputStream] = {
    Try {
      client.getObject(bucket, key).getObjectContent()
    }.toOption
  }

  def decompress(is: InputStream): Option[String] = {
    Try {
      val stream = new GZIPInputStream(is)

      IOUtils.toString(stream)
    }.toOption
  }
}
