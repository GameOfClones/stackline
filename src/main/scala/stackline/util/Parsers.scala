package stackline.util

import stackline.models.Product

import scala.util.Try

trait Parsers {
  def parseProductTsv(tsv: String): Option[List[Product]] = {
    println("parse the file")
    Some(tsv
      .split('\n')
      .map(_.split('\t'))
      .flatMap { row =>
        Try {
          Product(
            row(0),
            row(1),
            row(2),
            row(3),
            row(4),
            row(5))
        }.toOption
      }.toList)
  }
}
