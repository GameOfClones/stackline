FROM 1science/sbt:latest

COPY build.sbt /usr/src/app/

COPY project /usr/src/app/project

WORKDIR /usr/src/app

RUN sbt update

COPY . /usr/src/app

RUN sbt "; set assemblyOutputPath in assembly := file(\"./app.jar\"); assembly" && \
rm -rf /root/.ivy2 && \
ls | grep -v app.jar | xargs rm -rf

CMD ["java", "-server", "-XX:+UseG1GC", "-jar", "app.jar"]
