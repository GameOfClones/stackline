import Dependencies._
import sbt.Resolver

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "stackline",
    libraryDependencies ++= {
      val finchV = "0.17.0"
      val circeV = "0.9.1"

      Seq(
        "com.amazonaws"           % "aws-java-sdk"                  % "1.11.128",
        "com.github.finagle"      %%  "finch-core"                  % finchV,
        "com.github.finagle"      %%  "finch-circe"                 % finchV,
        "io.circe"                %%  "circe-core"                  % circeV,
        "io.circe"                %%  "circe-generic"               % circeV,
      )
    }
  )

resolvers += Resolver.sonatypeRepo("releases")

assemblyMergeStrategy in assembly := {
  case PathList("rootdoc.txt") => MergeStrategy.first
  case PathList("reference.conf") => MergeStrategy.concat
  case PathList("META-INF", xs @ _*) =>
    xs map {
      _.toLowerCase
    } match {
      case ("manifest.mf" :: Nil) | ("index.list" :: Nil) |
           ("dependencies" :: Nil) =>
        MergeStrategy.discard
      case ps @ (x :: xs)
        if ps.last.endsWith(".sf") || ps.last.endsWith(".dsa") =>
        MergeStrategy.discard
      case "plexus" :: xs => MergeStrategy.discard
      case "services" :: xs => MergeStrategy.filterDistinctLines
      case "notice.txt" :: Nil => MergeStrategy.discard
      case "license.txt" :: Nil => MergeStrategy.discard
      case "notice" :: Nil => MergeStrategy.discard
      case "license" :: Nil => MergeStrategy.discard
      case ("spring.schemas" :: Nil) | ("spring.handlers" :: Nil) =>
        MergeStrategy.filterDistinctLines
      case ps @ (x :: xs) if ps.last.endsWith("properties") =>
        MergeStrategy.filterDistinctLines
      case ps @ (x :: xs) if ps.last.startsWith("pom") => MergeStrategy.discard
      case ps @ (x :: xs) if ps.last.endsWith("so") => MergeStrategy.first
      case _ => MergeStrategy.deduplicate
    }
  case _ => MergeStrategy.first
}